/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafostercero2022;

import controlador.grafo.GrafoD;
import controlador.grafo.GrafoND;

/**
 *
 * @author sebitas
 */
public class GrafosTercero2022 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //V1 ---  3   4
        //V2 ----  1  
        //V3   --- 
        //V4  --   V3
        //V 5 ---- V2
        //GrafoD grafoD = new GrafoD(5);
        GrafoND grafoD = new GrafoND(5);
        grafoD.insertarArista(1, 3);
        grafoD.insertarArista(2, 1);
        grafoD.insertarArista(4, 3);
        grafoD.insertarArista(4, 5);
        grafoD.insertarArista(5, 2);
        System.out.println(grafoD.toString());
        //grafoD.existeArista(Integer.SIZE, Integer.SIZE);
       /* Vertice 1 --Vertice destino 3-- SP 
Vertice 2 --Vertice destino 1-- SP 
Vertice 3
Vertice 4 --Vertice destino 3-- SP  --Vertice destino 5-- SP 
Vertice 5 --Vertice destino 2-- SP */
    }
    
}
